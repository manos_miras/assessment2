﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Neil_and_Bens_Pizzeria
{
    public partial class MainWindow : Window
    {

        // Creates a new manager instance
        Manager manager = new Manager();

        // Constructor with no params
        public MainWindow()
        {

            InitializeComponent();

            manager.GetServers(); // Retrieves the Servers from database
            manager.GetDrivers(); // Retrieves the Drivers from database
            manager.GetDishes(); // Retrieves the Dishes from database

            cboBox_OrderType.SelectedValue = cbi1; // Initiates combobox

            // Populates cboBox_Server with the Servers
            foreach (Server server in manager.Servers)
            {
                ComboBoxItem item = new ComboBoxItem();
                //item.Name = server.Name;
                item.Content = server.Name;
                cboBox_Server.Items.Add(item);
                cboBox_Server.SelectedIndex = 0;
            }

            // Populates cboBox_Driver with the Drivers
            foreach (Driver driver in manager.Drivers)
            {
                ComboBoxItem item = new ComboBoxItem();
                //item.Name = driver.Name;
                item.Content = driver.Name;
                cboBox_Driver.Items.Add(item);
                cboBox_Driver.SelectedIndex = 0;
            }

            // Populates lst_AvailableDishes with the dishes
            foreach (MenuItem dish in manager.Dishes)
            {
                lst_AvailableDishes.Items.Add(dish.Description);
            }

        }
        // Constructor with the Manager param
        public MainWindow(Manager transferredManager)
        {
            manager = transferredManager;

            InitializeComponent();

            cboBox_OrderType.SelectedValue = cbi1; // Initiates combobox

            // Populates cboBox_Server with the Servers
            foreach (Server server in manager.Servers)
            {
                ComboBoxItem item = new ComboBoxItem();
                //item.Name = server.Name;
                item.Content = server.Name;
                cboBox_Server.Items.Add(item);
                cboBox_Server.SelectedIndex = 0;
            }

            // Populates cboBox_Driver with the Drivers
            foreach (Driver driver in manager.Drivers)
            {
                ComboBoxItem item = new ComboBoxItem();
                //item.Name = driver.Name;
                item.Content = driver.Name;
                cboBox_Driver.Items.Add(item);
                cboBox_Driver.SelectedIndex = 0;
            }

            foreach (MenuItem dish in manager.Dishes)
            {
                lst_AvailableDishes.Items.Add(dish.Description);
            }
        }

        // Enables or disables order info textboxes depending on what type of order is selected
        private void cboBox_OrderType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                // If sit-in
                if (cboBox_OrderType.SelectedValue == cbi1)
                {
                    txtBox_Table.IsEnabled = true;

                    cboBox_Driver.IsEnabled = false;
                    txtBox_CustomerName.IsEnabled = false;
                    txtBox_DeliveryAddress.IsEnabled = false;
            }
                // If delivery
                else if ((cboBox_OrderType.SelectedValue == cbi2))
                {
                    txtBox_Table.IsEnabled = false;

                    cboBox_Driver.IsEnabled = true;
                    txtBox_CustomerName.IsEnabled = true;
                    txtBox_DeliveryAddress.IsEnabled = true;
            }
        }

        // When the user clicks the >> button the selected dish from the available dishes is added to the ordered dishes.
        private void btn_AddToOrder_Click(object sender, RoutedEventArgs e)
        {
            lst_OrderedDishes.Items.Add(lst_AvailableDishes.SelectedItem);
        }

        // When the user clicks the << button the selected dish from the ordered dishes is removed from the ordered dishes.
        private void btn_RemoveFromOrder_Click(object sender, RoutedEventArgs e)
        {
            lst_OrderedDishes.Items.Remove(lst_OrderedDishes.SelectedItem);
        }

        // When user clicks the clear button the ordered dishes list gets cleared up.
        private void btn_ClearOrder_Click(object sender, RoutedEventArgs e)
        {
            lst_OrderedDishes.Items.Clear();
        }

        // Event Handler for the CreateBill button
        private void btn_CreateBill_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // if sit-in
                if (cboBox_OrderType.SelectedValue == cbi1)
                {
                    // Create new sit-in order
                    SitInOrder sitIn = new SitInOrder(manager, txtBox_Table.Text, cboBox_Server.SelectionBoxItem.ToString(), 
                        lst_OrderedDishes.Items.Cast<string>().ToList());
                    // Display the bill if no errors
                    if (sitIn.IsValidated)
                    {
                        Bill windowBill = new Bill(sitIn, GetCurrentServer(cboBox_Server.SelectionBoxItem.ToString()));
                        windowBill.ShowDialog();
                    }
                }

                // if delivery
                else if ((cboBox_OrderType.SelectedValue == cbi2))
                {
                    // Create a new delivery order
                    DeliveryOrder delivery = new DeliveryOrder(manager, txtBox_CustomerName.Text, txtBox_DeliveryAddress.Text, 
                        cboBox_Server.SelectionBoxItem.ToString(),cboBox_Driver.SelectionBoxItem.ToString() , 
                        lst_OrderedDishes.Items.Cast<string>().ToList());
                    // Display the bill if no errors
                    if (delivery.IsValidated)
                    {
                        Bill windowBill = new Bill(delivery, GetCurrentServer(cboBox_Server.SelectionBoxItem.ToString()));
                        windowBill.ShowDialog();
                    }
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
        // Event handler for the ManagerView button
        // Creates a new manager window and opens it
        private void btn_ManagerView_Click(object sender, RoutedEventArgs e)
        {
            ManagerWindow managerWindow = new ManagerWindow(manager);
            managerWindow.Show();
            this.Close();
        }

        private Server GetCurrentServer(string serverName)
        {
            foreach (Server currentServer in manager.Servers)
            {
                if (currentServer.Name == serverName)
                    return currentServer;
            }
            return null;
        }
    }
}
