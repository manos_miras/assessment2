﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neil_and_Bens_Pizzeria
{
    /*  
     * The purpose of the MenuItem class is to handle creation of MenuItem objects 
     * and hold information that is relevant to every MenuItem.
    
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    public class MenuItem
    {
        private string _description; // The name of the MenuItem
        private bool _vegeterian; // Item is vegeterian or not
        private int _price; // Price of the MenuItem

        public string Description
        {
            get { return _description; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Description field may not be blank.");
                }
                _description = value;
            } 
        }

        public bool Vegeterian
        {
            get { return _vegeterian; }
            set { _vegeterian = value; }
        }

        public int Price
        {
            get { return _price; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Price cannot be a negative number.");
                }
                if (value > 100000)
                {
                    throw new ArgumentException("Price may not exceed £100 (100,000 pence)");
                }
                _price = value;
            }
        }

        // Constructor
        public MenuItem()
        { 
        
        }

        // Constructor with parameters
        public MenuItem(string description, string price, string vegeterian, Manager manager)
        {
            Description = description;

            if (IsNumeric(price, "Price must be an integer"))
                Price = int.Parse(price);
            // Check if yes or no in combobox to assign true or false respectively
            if (vegeterian == "Y" || vegeterian == "Yes" || vegeterian == "1" || vegeterian == "True")
                Vegeterian = true;
            else if (vegeterian == "N" || vegeterian == "No" || vegeterian == "0" || vegeterian == "False")
                Vegeterian = false;

            manager.Dishes.Add(this);
        }

        //Checks if a value is numeric or not
        private static bool IsNumeric(string input, string ExceptionString)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (!int.TryParse(input, out output))

                throw new ArgumentException(ExceptionString);
            else
                return true;
        }

        // Takes the current price and converts it from pence to pounds, returns a string with the £ symbol added.
        public string ToPounds()
        {
            double currentPrice =  Convert.ToDouble(this.Price)/1000;
            
            return "£" + currentPrice.ToString();
        }
    }

}
