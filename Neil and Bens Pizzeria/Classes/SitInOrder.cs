﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Neil_and_Bens_Pizzeria
{
    /*  
     * The purpose of the SitInOrder class is to handle creation of SitInOrder objects 
     * and to store information that is relevant to every SitInOrder.
    
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    public class SitInOrder : IOrder
    {
        private List<MenuItem> _menuItems; // List of Dishes ordered
        private int _table; // Order table
        private string _serverName; // Server that undertook the order
        private int _price; // Total order price
        private bool _isValidated; // Used to verify validation occurence

        public List<MenuItem> MenuItems
        {
            get { return _menuItems; }
            set { _menuItems = value; }
        }

        public int Table
        {
            get { return _table; }
            set
            {
                if (value < 1 || value > 10)
                {
                    throw new ArgumentException("Table value out of range (Acceptable table range: 1 - 10)");
                }
                _table = value;
            }
        }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }

        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public bool IsValidated
        {
            get { return _isValidated; }
            set { _isValidated = value; }
        }
        // Constructor with parameters
        public SitInOrder(Manager manager, string currentTable, string currentServer, List<string> orderedDishesList)
        {
            MenuItems = new List<MenuItem>();

            IsValidated = false; // Validation has not taken place
            try
            {

                int totalPrice = 0; // Stores the total Price sum

                foreach (MenuItem dish in manager.Dishes)
                {
                    foreach (string currentDish in orderedDishesList)
                    {
                        if (dish.Description == currentDish)
                        {
                            this.MenuItems.Add(dish);
                            totalPrice += dish.Price;
                        }
                    }
                }
                // Update current sitIn Order properties
                if (IsNumeric(currentTable, "Illegal value found inside Table field, you must insert a number from 1 to 10."))
                    this.Table = int.Parse(currentTable);

                this.ServerName = currentServer;

                this.Price = totalPrice;
                // Add current sitIn Order to list SitInOrders 
                manager.SitInOrders.Add(this);

                IsValidated = true; // Validation was successful
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        //Checks if a value is numeric or not
        private static bool IsNumeric(string input, string ExceptionString)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (!int.TryParse(input, out output))

                throw new ArgumentException(ExceptionString);
            else
                return true;
        }
    }
}
