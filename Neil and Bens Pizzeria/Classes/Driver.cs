﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Neil_and_Bens_Pizzeria
{
    /*  
     * The purpose of the Driver class is to handle creation of Driver objects 
     * and hold information that is relevant to every Driver.
     * 
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    public class Driver : IStaff
    {
        private string _name; // Name of Driver
        private int _staffID; // Driver's StaffID
        private string _carRegistration; //Driver's car registration
        private bool _isValidated; // Used to verify validation occurence

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == "")
                    throw new ArgumentException("Driver name may not be blank.");
                _name = value;
            }
        }

        public int StaffID
        {
            get { return _staffID; }
            set { _staffID = value; }
        }

        public bool isValidated
        {
            get { return _isValidated; }
            set { _isValidated = value; }
        }

        public string CarRegistration
        {
            get { return _carRegistration; }
            set
            {
                if (value == "")
                    throw new ArgumentException("Driver's registration may not be blank.");
                _carRegistration = value;
            }
        }

        // Constructor with parameters
        public Driver(string name, string staffID, string carRegistration, Manager manager)
        {
            isValidated = false; // Validation has not taken place

            try
            {
                // Assign properties
                Name = name;
                CarRegistration = carRegistration;
                if (IsValidID(staffID, manager))
                {
                    if (IsNumeric(staffID, "Non numeric value inserted into StaffID field."))
                        StaffID = int.Parse(staffID);
                }
                // Add to list of Drivers
                manager.Drivers.Add(this);

                isValidated = true; // Validation was successful
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

        }

        //Checks if a value is numeric or not
        public static bool IsNumeric(string input, string ExceptionString)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (!int.TryParse(input, out output))

                throw new ArgumentException(ExceptionString);
            else
                return true;
        }

        // Checks if the ID specified is valid.
        public bool IsValidID(string staffID, Manager manager)
        {
            foreach (Server currentServer in manager.Servers)
            {
                if (currentServer.StaffID.ToString() == staffID)
                    throw new ArgumentException("Staff ID: " + staffID + ", is already assigned to " + currentServer.Name);
            }

            foreach (Driver currentDriver in manager.Drivers)
            {
                if (currentDriver.StaffID.ToString() == staffID)
                    throw new ArgumentException("Staff ID: " + staffID + ", is already assigned to " + currentDriver.Name);
            }

            return true;
        }
    }
}
