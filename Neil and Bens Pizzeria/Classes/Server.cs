﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Neil_and_Bens_Pizzeria
{
    /*  
     * The purpose of the Server class is to handle creation of Server objects 
     * and hold information that is relevant to every Server.
     * 
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    public class Server: IStaff
    {
        private string _name;
        private int _staffID;
        private bool _isValidated; // Used to verify validation occurence

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == "")
                    throw new ArgumentException("Server name may not be blank.");
                _name = value;
            }
        }

        public int StaffID
        {
            get { return _staffID; }
            set { _staffID = value; }
        }

        public bool IsValidated
        {
            get { return _isValidated; }
            set { _isValidated = value; }
        }

        // Constructor
        public Server()
        { 
            
        }

        // Constructor with parameters
        public Server(string name, string staffID, Manager manager)
        {
            IsValidated = false; // Validation has not taken place

            try
            {
                // Assign properties
                Name = name;
                if (IsValidID(staffID, manager))
                {
                    if (IsNumeric(staffID, "Non numeric value inserted into StaffID field."))
                        StaffID = int.Parse(staffID);
                }

                // Add to list of Servers
                manager.Servers.Add(this);

                IsValidated = true; // Validation was successful
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        public string CreateBill(int price, bool isDelivery)
        {
            double finalPrice = Convert.ToDouble(price); // Convert to the price to double, in order to display properly
            if (isDelivery)
                finalPrice += finalPrice * 0.15; // Add delivery fee to the price
            // Convert final price to Pounds from pence and add the £ symbol
            string priceOutput = "£" + finalPrice / 1000;
            // Return as a string, ready to be displayed on a label or other control
            return priceOutput;
        }

        //Checks if a value is numeric or not
        public static bool IsNumeric(string input, string ExceptionString)
        {
            //Int output: Used to hold in the output from TryParse
            int output;
            //Checks if entered value is numeric or not
            if (!int.TryParse(input, out output))

                throw new ArgumentException(ExceptionString);
            else
                return true;
        }

        // Checks if the ID specified is valid.
        public bool IsValidID(string staffID, Manager manager)
        {
            foreach (Server currentServer in manager.Servers)
            {
                if (currentServer.StaffID.ToString() == staffID)
                    throw new ArgumentException("Staff ID: " + staffID + ", is already assigned to " + currentServer.Name);
            }

            foreach (Driver currentDriver in manager.Drivers)
            {
                if (currentDriver.StaffID.ToString() == staffID)
                    throw new ArgumentException("Staff ID: " + staffID + ", is already assigned to " + currentDriver.Name);
            }

            return true;
        }
    }
}
