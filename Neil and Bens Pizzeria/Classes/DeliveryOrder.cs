﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Neil_and_Bens_Pizzeria
{
    /*  
     * The purpose of the DeliveryOrder class is to handle creation of DeliveryOrder objects 
     * and hold information that is relevant to every DeliveryOrder.
    
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    public class DeliveryOrder :IOrder
    {
        private List<MenuItem> _menuItems; // List of Dishes ordered
        private string _customerName; // Customer name
        private string _deliveryAddress; // Customer delivery address

        private string _serverName; // Server that undertook the order
        private string _driverName; // Driver that will deliver order
        private int _price; // Total order price
        private bool _isValidated; // Used to verify validation occurence

        public List<MenuItem> MenuItems
        {
            get { return _menuItems; }
            set { _menuItems = value; }
        }

        public string CustomerName
        {
            get { return _customerName; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Customer name may not be blank.");
                }
                _customerName = value;
            }
        }

        public string DeliveryAddress
        {
            get { return _deliveryAddress; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Delivery address may not be blank.");
                }
                _deliveryAddress = value;
            }
        }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }

        public string DriverName
        {
            get { return _driverName; }
            set { _driverName = value; }
        }

        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public bool IsValidated
        {
            get { return _isValidated; }
            set { _isValidated = value; }
        }

        // Constructor with parameters
        public DeliveryOrder(Manager manager, string customerName, string deliveryAddress,
             string currentServer, string currentDriver, List<string> orderedDishesList)
        {
            MenuItems = new List<MenuItem>();
            IsValidated = false; // Validation has not taken place
            try
            {
                int totalPrice = 0; // Stores the total Price sum

                foreach (MenuItem dish in manager.Dishes)
                {
                    foreach (string currentDish in orderedDishesList)
                    {
                        if (dish.Description == currentDish)
                        {
                            this.MenuItems.Add(dish);
                            totalPrice += dish.Price;
                        }
                    }

                }

                // Update current delivery Order properties
                this.CustomerName = customerName;
                this.DeliveryAddress = deliveryAddress;
                this.ServerName = currentServer;
                this.DriverName = currentDriver;
                this.Price = totalPrice;

                // Add current delivery Order to list DeliveryOrders 
                manager.DeliveryOrders.Add(this);

                IsValidated = true; // Validation was successful
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

    }
}
