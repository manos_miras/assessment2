﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Neil_and_Bens_Pizzeria
{
    public partial class ManagerWindow : Window
    {
        #region Constructor and other
        // Creates a new manager instance
        Manager manager = new Manager();

        // Constructor with Manager parameter.
        public ManagerWindow(Manager transferredManager)
        {
            InitializeComponent();
            // Assigns the transferredManager data to manager
            manager = transferredManager;

            // Populates lstBox_Dishes with the description of each dish
            foreach (MenuItem dish in manager.Dishes)
            {
                lstBox_Dishes.Items.Add(dish.Description);
            }

            // Populates lstBox_Servers with the name of each server
            foreach (Server server in manager.Servers)
            {
                lstBox_Servers.Items.Add(server.Name);
            }

            // Populates lstBox_Drivers with the name of each driver
            foreach (Driver driver in manager.Drivers)
            {
                lstBox_Drivers.Items.Add(driver.Name);
            }
        }

        private void btn_Back_Click(object sender, RoutedEventArgs e)
        {

            //manager.SetServers(); // Stores servers to database
            MainWindow mainWindow = new MainWindow(manager);
            this.Close();

            mainWindow.Show();
        }

        #endregion

        #region Dishes


        // Event handler for when the selection inside the dishes listbox is changed
        private void lstBox_Dishes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Checks the description of the item and matches it to the correct price and vegeterian properties.
            foreach (MenuItem dish in manager.Dishes)
            {
                try
                {
                    // Clears the textboxes if the item gets removed
                    if (lstBox_Dishes.SelectedValue == null)
                    {
                        ClearTextBoxes(txtBox_Price, txtBox_isVegan);
                    }
                    else
                    {
                        if (dish.Description == lstBox_Dishes.SelectedValue.ToString())
                        {
                            txtBox_Price.Text = dish.ToPounds();
                            txtBox_isVegan.Text = dish.Vegeterian.ToString();
                        }
                    }
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
            // Clear the fields
            ClearTextBoxes(txtBox_Dishes_Description, txtBox_Dishes_Price); // Clears the textboxes
            cboBox_Dishes_Vegeterian.SelectedIndex = -1; // Sets selection to nothing
            btn_Dishes_Add.Content = "Add"; // Resets the content to "Add"
            txtBox_Dishes_Description.IsEnabled = true; // Re-enables the Dish description textbox
        }

        // Event handler for when the Add button inside the dishes tab is clicked
        private void btn_Dishes_Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Checks if the user is in "add mode" or "edit mode"
                if (btn_Dishes_Add.Content.ToString() == "Add")
                {
                    // User is in add mode
                    // Create new MenuItem
                    MenuItem menuAdd = new MenuItem(txtBox_Dishes_Description.Text, txtBox_Dishes_Price.Text, 
                        cboBox_Dishes_Vegeterian.SelectionBoxItem.ToString(), manager);

                    // Update the listbox to show the added dish description
                    lstBox_Dishes.Items.Add(menuAdd.Description);

                    // Clear the fields
                    ClearTextBoxes(txtBox_Dishes_Description, txtBox_Dishes_Price); // Clears the textboxes
                    cboBox_Dishes_Vegeterian.SelectedIndex = -1; // Sets selection to nothing
                }
                else
                {
                    // User is in edit mode
                    foreach (MenuItem dish in manager.Dishes)
                    {
                        if (dish.Description == lstBox_Dishes.SelectedValue.ToString())
                        {

                            // Assign its properties
                            dish.Description = txtBox_Dishes_Description.Text;
                            dish.Price = int.Parse(txtBox_Dishes_Price.Text);

                            // Check if yes or no in combobox to assign true or false respectively
                            if (cboBox_Dishes_Vegeterian.SelectedItem == Y)
                                dish.Vegeterian = true;
                            else if (cboBox_Dishes_Vegeterian.SelectedItem == N)
                                dish.Vegeterian = false;

                            // Update the price and vegan textboxes
                            txtBox_Price.Text = dish.ToPounds();
                            txtBox_isVegan.Text = dish.Vegeterian.ToString();

                            // Make the description editable again
                            txtBox_Dishes_Description.IsEnabled = true;

                            // Empty textboxes and comboboxes
                            ClearTextBoxes(txtBox_Dishes_Description, txtBox_Dishes_Price);
                            ClearComboBoxes(cboBox_Dishes_Vegeterian);
                        }
                    }
                }
                btn_Dishes_Add.Content = "Add"; // Set the content to Add
            }

            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        // Event handler for when the Remove button inside the dishes tab is clicked
        private void btn_Dishes_Remove_Click(object sender, RoutedEventArgs e)
        {
            // Removes from the list
            manager.Dishes.RemoveAt(lstBox_Dishes.SelectedIndex);
            // Removes the selected item from the listBox
            lstBox_Dishes.Items.Remove(lstBox_Dishes.SelectedItem);

            // Make the description editable again
            txtBox_Dishes_Description.IsEnabled = true;
        }

        // Event handler for when the Clear button inside the dishes tab is clicked
        private void btn_Dishes_Clear_Click(object sender, RoutedEventArgs e)
        {
            // Clears all of the list
            manager.Dishes.Clear();
            // Removes all of the items from the listBox
            ClearListBoxes(lstBox_Dishes);
            // Clear the price and vegeterian info text boxes
            ClearTextBoxes(txtBox_Price, txtBox_isVegan);

            // Make the description editable again
            txtBox_Dishes_Description.IsEnabled = true;
        }

        // Event handler for when the Edit button inside the dishes tab is clicked
        private void btn_Dishes_Edit_Click(object sender, RoutedEventArgs e)
        {
            // Clear the fields
            ClearTextBoxes(txtBox_Dishes_Description, txtBox_Dishes_Price); // Clears the textboxes
            cboBox_Dishes_Vegeterian.SelectedIndex = -1; // Sets selection to nothingclearPropertyControls();

            // Make description property non editable
            txtBox_Dishes_Description.IsEnabled = false;

            // Update the content so the user knows he is now editing
            btn_Dishes_Add.Content = "Update";

            // Grabs the properties and displays them in the relevant controls
            foreach (MenuItem dish in manager.Dishes)
            {
                try
                {
                    {
                        if (dish.Description == lstBox_Dishes.SelectedValue.ToString())
                        {
                            txtBox_Dishes_Description.Text = dish.Description;
                            txtBox_Dishes_Price.Text = dish.Price.ToString();
                            // Used to display a Y/N on the combobox in place of true/false respectively
                            if (dish.Vegeterian == true)
                                cboBox_Dishes_Vegeterian.SelectedItem = Y;
                            else
                                cboBox_Dishes_Vegeterian.SelectedItem = N;
                        }
                    }
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }
        #endregion

        #region General

        // Event handler for btn_General_SitInOrders click
        // Lists all sit in orders placed
        private void btn_General_SitInOrders_Click(object sender, RoutedEventArgs e)
        {
            // Adds a "title" to the list
            lstBox_General.Items.Add("- LISTING ALL SIT IN ORDERS PLACED:");

            foreach (SitInOrder currentOrder in manager.SitInOrders)
            {
                lstBox_General.Items.Add("Server name: " + currentOrder.ServerName + ", Table no: " + currentOrder.Table + ", Price: £" + currentOrder.Price / 1000);
            }
        }

        // Event handler for btn_General_DeliveryOrders click
        // Lists all delivery orders placed
        private void btn_General_DeliveryOrders_Click(object sender, RoutedEventArgs e)
        {
            // Adds a "title" to the list
            lstBox_General.Items.Add("- LISTING ALL DELIVERY ORDERS PLACED:");

            foreach (DeliveryOrder currentOrder in manager.DeliveryOrders)
            {
                lstBox_General.Items.Add("Server name: " + currentOrder.ServerName + ", Driver Name: " + currentOrder.DriverName + ", Customer Name: " + currentOrder.CustomerName + ", Price: £" + (currentOrder.Price + currentOrder.Price * 0.15) / 1000);
            }
        }

        private void btn_General_OrdersByServer_Click(object sender, RoutedEventArgs e)
        {
            // Stores the user inserted server name
            string serverName = txtBox_General_ServerName.Text;

            // Adds a "title" to the list
            lstBox_General.Items.Add("- LISTING ALL DELIVERY ORDERS PLACED BY SERVER: " + serverName);

            // Lists all sit in orders placed
            foreach (SitInOrder currentOrder in manager.SitInOrders)
            {
                if (serverName == currentOrder.ServerName)
                    lstBox_General.Items.Add("Server name: " + currentOrder.ServerName + ", Table no: " + currentOrder.Table + ", Price: £" + currentOrder.Price / 1000);
            }

            // Lists all delivery orders placed
            foreach (DeliveryOrder currentOrder in manager.DeliveryOrders)
            {
                if (serverName == currentOrder.ServerName)
                    lstBox_General.Items.Add("Server name: " + currentOrder.ServerName + ", Driver Name: " + currentOrder.DriverName + ", Customer Name: " + currentOrder.CustomerName + ", Price: £" + (currentOrder.Price + currentOrder.Price * 0.15)/1000);
            }
        }

        // Event handler for btn_General_MenuItemsAndQuantities click
        // Lists all menu items and the quantities of each ordered
        private void btn_General_MenuItemsAndQuantities_Click(object sender, RoutedEventArgs e)
        {
            foreach (MenuItem currentDish in manager.Dishes)
                lstBox_General.Items.Add(currentDish.Description + ", x"  + quantityOrdered(currentDish) + " Ordered");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Apply changes to the local database?", "Question", MessageBoxButton.YesNo,
                MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
                // do no stuff
            }
            else if (result == MessageBoxResult.Yes)
            {
                // do yes stuff
                manager.SetServers(); // Stores servers to database
                manager.SetDrivers(); // Stores drivers to database
                manager.SetDishes(); // Stores dishes to database
            }
        }

        #endregion

        #region Methods

        // Used to calculate the quantities of each ordered item
        // Returns an integer, takes a MenuItem as a parameter
        private int quantityOrdered(MenuItem currentDish)
        {
            int orderCount = 0; // Stores the order count

            // Check all sit in orders placed
            foreach (SitInOrder currentOrder in manager.SitInOrders)
            {
                for (int counter = 0; counter < currentOrder.MenuItems.Count; counter++)
                {
                    if (currentDish == currentOrder.MenuItems[counter])
                        orderCount++;
                }
            }

            // Check all delivery orders placed
            foreach (DeliveryOrder currentOrder in manager.DeliveryOrders)
            {
                for (int counter = 0; counter < currentOrder.MenuItems.Count; counter++)
                {
                    if (currentDish == currentOrder.MenuItems[counter])
                        orderCount++;
                }
            }

            return orderCount;
        }

        // Clears textboxes
        private void ClearTextBoxes(params TextBox[] list)
        {
            foreach (TextBox textBox in list)
            {
                textBox.Clear();
            }
        }

        // Clears listboxes
        private void ClearListBoxes(params ListBox[] list)
        {
            foreach (ListBox listBox in list)
            {
                listBox.Items.Clear();
            }
        }

        // Clears comboboxes
        private void ClearComboBoxes(params ComboBox[] list)
        {
            foreach (ComboBox comboBox in list)
            {
                comboBox.Items.Clear();
            }
        }

        #endregion methods

        #region Servers

        private void btn_Servers_Remove_Click(object sender, RoutedEventArgs e)
        {
            // Removes from the list
            manager.Servers.RemoveAt(lstBox_Servers.SelectedIndex);
            // Removes the selected item from the listBox
            lstBox_Servers.Items.Remove(lstBox_Servers.SelectedItem);

            // Make the description editable again
            txtBox_Servers_Name.IsEnabled = true;
        }

        private void btn_Servers_Clear_Click(object sender, RoutedEventArgs e)
        {
            // Clears all of the list
            manager.Servers.Clear();
            // Removes all of the items from the listBox
            ClearListBoxes(lstBox_Servers);
            // Clears the info textboxes
            ClearTextBoxes(txtBox_ServersInfo_StaffID);

            // Make the description editable again
            txtBox_Servers_Name.IsEnabled = true;
        }

        private void btn_Servers_Add_Click(object sender, RoutedEventArgs e)
        {
            // Checks if the user is in "add mode" or "edit mode"
            if (btn_Servers_Add.Content.ToString() == "Add")
            {
                // Create new Server
                Server serverAdd = new Server(txtBox_Servers_Name.Text, txtBox_Servers_StaffID.Text, manager);

                // Checks if validation has taken place first
                if (serverAdd.IsValidated)
                {
                    // Update the listbox to show the added Server name
                    lstBox_Servers.Items.Add(serverAdd.Name);

                    // Clear the fields
                    ClearTextBoxes(txtBox_Servers_Name, txtBox_Servers_StaffID); // Clears the textboxes
                }
            }
            else
            {
                // User is in edit mode
                foreach (Server server in manager.Servers)
                {
                    if (server.Name == lstBox_Servers.SelectedValue.ToString())
                    {
                        // Assign its properties
                        server.Name = txtBox_Servers_Name.Text;
                        // Performs some validation before assigning value
                        try
                        {
                            if (server.IsValidID(txtBox_Servers_StaffID.Text, manager) && Server.IsNumeric(txtBox_Servers_StaffID.Text, 
                                "Non numeric value inserted into StaffID field."))
                                server.StaffID = int.Parse(txtBox_Servers_StaffID.Text);
                            }
                        catch (Exception excep)
                        {
                            MessageBox.Show(excep.Message);
                        }

                        // Updates the info textboxes
                        txtBox_ServersInfo_StaffID.Text = server.StaffID.ToString();

                        // Make the description editable again
                        txtBox_Servers_Name.IsEnabled = true;

                        // Empty textboxes
                        ClearTextBoxes(txtBox_Servers_Name, txtBox_Servers_StaffID);
                    }
                }
            }
            btn_Servers_Add.Content = "Add"; // Set the content to Add
        }

        private void btn_Servers_Edit_Click(object sender, RoutedEventArgs e)
        {
            // Clear the fields
            ClearTextBoxes(txtBox_Servers_Name, txtBox_Servers_StaffID); // Clears the textboxes

            // Make name property non editable
            txtBox_Servers_Name.IsEnabled = false;

            // Update the content so the user knows he is now editing
            btn_Servers_Add.Content = "Update";

            // Grabs the properties and displays them in the relevant controls
            foreach (Server server in manager.Servers)
            {
                if (server.Name == lstBox_Servers.SelectedValue.ToString())
                {
                    txtBox_Servers_Name.Text = server.Name;
                    txtBox_Servers_StaffID.Text = server.StaffID.ToString();
                }
            }
        }

        private void lstBox_Servers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Checks the name of the server and matches it to the correct StaffID.
            foreach (Server server in manager.Servers)
            {
                // Clears the textboxes if the item gets removed
                if (lstBox_Servers.SelectedValue == null)
                {
                    ClearTextBoxes(txtBox_ServersInfo_StaffID);
                }
                else
                {
                    if (server.Name == lstBox_Servers.SelectedValue.ToString())
                    {
                        txtBox_ServersInfo_StaffID.Text = server.StaffID.ToString();
                    }
                }
            }
            ClearTextBoxes(txtBox_Servers_Name, txtBox_Servers_StaffID);
            btn_Servers_Add.Content = "Add"; // Resets the content to "Add"
            txtBox_Servers_Name.IsEnabled = true; // Re-enables the Server name textbox
        }

        #endregion

        #region Drivers

        private void btn_Drivers_Remove_Click(object sender, RoutedEventArgs e)
        {
            // Removes from the list
            manager.Drivers.RemoveAt(lstBox_Drivers.SelectedIndex);
            // Removes the selected item from the listBox
            lstBox_Drivers.Items.Remove(lstBox_Drivers.SelectedItem);
            // Clears the info textboxes
            ClearTextBoxes(txtBox_DriversInfo_StaffID, txtBox_DriversInfo_Registration);

            // Make the description editable again
            txtBox_Drivers_Name.IsEnabled = true;
        }

        private void btn_Drivers_Clear_Click(object sender, RoutedEventArgs e)
        {
            // Clears all of the list
            manager.Drivers.Clear();
            // Removes all of the items from the listBox
            ClearListBoxes(lstBox_Drivers);
            // Clears the info textboxes
            ClearTextBoxes(txtBox_DriversInfo_StaffID, txtBox_DriversInfo_Registration);

            // Make the description editable again
            txtBox_Drivers_Name.IsEnabled = true;
        }

        private void btn_Drivers_Add_Click(object sender, RoutedEventArgs e)
        {
            // Checks if the user is in "add mode" or "edit mode"
            if (btn_Drivers_Add.Content.ToString() == "Add")
            {
                // User is in add mode
                // Create new driver
                Driver driverAdd = new Driver(txtBox_Drivers_Name.Text, txtBox_Drivers_StaffID.Text, txtBox_Drivers_Registration.Text, manager);

                if (driverAdd.isValidated)
                {
                    // Update the listbox to show the added Driver name
                    lstBox_Drivers.Items.Add(driverAdd.Name);

                    // Clear the fields
                    ClearTextBoxes(txtBox_Drivers_Name, txtBox_Drivers_StaffID, txtBox_Drivers_Registration); // Clears the textboxes
                }
            }
            else
            {
                // User is in edit mode
                foreach (Driver driver in manager.Drivers)
                {
                    if (driver.Name == lstBox_Drivers.SelectedValue.ToString())
                    {
                        // Assign its properties
                        driver.Name = txtBox_Drivers_Name.Text;

                        // Performs some validation before assigning value
                        try
                        {
                            if (driver.IsValidID(txtBox_Drivers_StaffID.Text, manager) && Driver.IsNumeric(txtBox_Drivers_StaffID.Text,
                                "Non numeric value inserted into StaffID field."))
                                driver.StaffID = int.Parse(txtBox_Drivers_StaffID.Text);
                        }
                        catch (Exception excep)
                        {
                            MessageBox.Show(excep.Message);
                        }

                        driver.CarRegistration = txtBox_Drivers_Registration.Text;

                        // Updates the info textboxes
                        txtBox_DriversInfo_StaffID.Text = driver.StaffID.ToString();
                        txtBox_DriversInfo_Registration.Text = driver.CarRegistration;

                        // Make the name editable again
                        txtBox_Drivers_Name.IsEnabled = true;

                        // Empty textboxes
                        ClearTextBoxes(txtBox_Drivers_Name, txtBox_Drivers_StaffID, txtBox_Drivers_Registration);
                    }

                }
            }
            btn_Drivers_Add.Content = "Add"; // Set the content to Add
        }

        private void btn_Drivers_Edit_Click(object sender, RoutedEventArgs e)
        {
            // Clear the fields
            ClearTextBoxes(txtBox_Drivers_Name, txtBox_Drivers_StaffID, txtBox_Drivers_Registration); // Clears the textboxes

            // Make name property non editable
            txtBox_Drivers_Name.IsEnabled = false;

            // Update the content so the user knows he is now editing
            btn_Drivers_Add.Content = "Update";

            // Grabs the properties and displays them in the relevant controls
            foreach (Driver driver in manager.Drivers)
            {
                if (driver.Name == lstBox_Drivers.SelectedValue.ToString())
                {
                    txtBox_Drivers_Name.Text = driver.Name;
                    txtBox_Drivers_StaffID.Text = driver.StaffID.ToString();
                    txtBox_Drivers_Registration.Text = driver.CarRegistration;
                }
            }
        }

        private void lstBox_Drivers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Checks the name of the driver and matches it to the correct StaffID.
            foreach (Driver driver in manager.Drivers)
            {
                // Clears the textboxes if the item gets removed
                if (lstBox_Drivers.SelectedValue == null)
                {
                    ClearTextBoxes(txtBox_DriversInfo_StaffID, txtBox_DriversInfo_Registration);
                }
                else
                {
                    if (driver.Name == lstBox_Drivers.SelectedValue.ToString())
                    {
                        txtBox_DriversInfo_StaffID.Text = driver.StaffID.ToString();
                        txtBox_DriversInfo_Registration.Text = driver.CarRegistration;
                    }
                }
            }
            ClearTextBoxes(txtBox_Drivers_Name, txtBox_Drivers_StaffID, txtBox_Drivers_Registration);
            btn_Drivers_Add.Content = "Add"; // Resets the content to "Add"
            txtBox_Drivers_Name.IsEnabled = true; // Re-enables the Driver name textbox
        }

        #endregion drivers

    }
}