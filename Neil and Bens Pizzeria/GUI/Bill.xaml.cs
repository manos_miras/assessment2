﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Neil_and_Bens_Pizzeria
{
    public partial class Bill : Window
    {
        // Constructor for SitInOrder
        public Bill(SitInOrder sitInOrder, Server server)
        {
            InitializeComponent();

            foreach (MenuItem currentDish in sitInOrder.MenuItems)
            {
                txtBox_OrderedItems.Text += currentDish.Description + "\n";
            }
            lbl_Total.Content = server.CreateBill(sitInOrder.Price, false);
        }
        // Constructor for DeliveryOrder
        public Bill(DeliveryOrder deliveryOrder, Server server)
        {
            InitializeComponent();

            foreach (MenuItem currentDish in deliveryOrder.MenuItems)
            {
                txtBox_OrderedItems.Text += currentDish.Description + "\n";
            }
            lbl_Total.Content = server.CreateBill(deliveryOrder.Price, true);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
