﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neil_and_Bens_Pizzeria
{
    public interface IOrder
    {
        List<MenuItem> MenuItems
        {
            get;
            set;
        }
    }
}
