﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows;

namespace Neil_and_Bens_Pizzeria
{
    public class Manager
    {
        // List of Servers
        private List<Server> _servers = new List<Server>();
        // List of Dishes
        private List<MenuItem> _dishes = new List<MenuItem>();
        // List of Drivers
        private List<Driver> _drivers = new List<Driver>();
        // List of SitIn Orders
        private List<SitInOrder> _sitInOrders = new List<SitInOrder>();
        // List of Delivery Orders
        private List<DeliveryOrder> _deliveryOrders = new List<DeliveryOrder>();

        public List<Server> Servers
        {
            get { return _servers; }
            set { _servers = value; }
        }

        public List<MenuItem> Dishes
        {
            get { return _dishes; }
            set { _dishes = value; }
        }

        public List<Driver> Drivers
        {
            get { return _drivers; }
            set { _drivers = value; }
        }

        public List<SitInOrder> SitInOrders
        {
            get { return _sitInOrders; }
            set { _sitInOrders = value; }
        }

        public List<DeliveryOrder> DeliveryOrders
        {
            get { return _deliveryOrders; }
            set { _deliveryOrders = value; }
        }

        // Constructor
        public Manager()
        {
            // Instantiate Lists
            Servers = new List<Server>();
            Dishes = new List<MenuItem>();
            Drivers = new List<Driver>();
            SitInOrders = new List<SitInOrder>();
            DeliveryOrders = new List<DeliveryOrder>();

            //// Sample Servers Init
            //Server serv1 = new Server("Jack", "30", this);
            //Server serv2 = new Server("Adam", "31", this);
            //Server serv3 = new Server("Nick", "32", this);

            //// Sample Drivers Init
            //Driver driv1 = new Driver("Jim", "4", "AH 151", this);
            //Driver driv2 = new Driver("Matthew", "5", "CH 174", this);
            //Driver driv3 = new Driver("Andrew", "6", "XX 839", this);

            //// Sample Dishes Init
            //MenuItem menu1 = new MenuItem("Mushroom Pizza", "10000", "Y", this);
            //MenuItem menu2 = new MenuItem("Bacon and Ham Pizza", "12000", "N", this);
            //MenuItem menu3 = new MenuItem("Margherita Pizza", "7000", "N", this);
        }

        // Retrieves the servers from the local database
        public void GetServers()
        {
            SqlConnection connection =
                new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;");
            connection.Open();
            SqlCommand command = new SqlCommand(
                "SELECT Name, Staff_ID FROM Server", connection);
            SqlDataReader sdr = command.ExecuteReader();
            while (sdr.Read())
            {
                Server server = new Server(sdr["Name"].ToString().Trim(), sdr["Staff_ID"].ToString(), this);
            }
            sdr.Close();
        }

        // Stores the servers to the local database
        public void SetServers()
        {
            SqlConnection connection =
                new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;");
            connection.Open();

            SqlCommand clearCommand = new SqlCommand(
                "TRUNCATE TABLE Server;");
            clearCommand.Connection = connection;
            clearCommand.ExecuteNonQuery();

            foreach (Server currentServer in this.Servers)
            {
                try
                {
                    SqlCommand command = new SqlCommand(
                        "INSERT into Server (Name, Staff_ID) VALUES (@Name, @Staff_ID)");
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Name", currentServer.Name);
                    command.Parameters.AddWithValue("@Staff_ID", currentServer.StaffID);
                    command.ExecuteNonQuery();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        // Retrieves the drivers from the local database
        public void GetDrivers()
        {
            SqlConnection connection =
                new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;");
            connection.Open();
            SqlCommand command = new SqlCommand(
                "SELECT Name, Staff_ID, Registration FROM Driver", connection);
            SqlDataReader sdr = command.ExecuteReader();
            while (sdr.Read())
            {
                Driver driver = new Driver(sdr["Name"].ToString().Trim(), sdr["Staff_ID"].ToString(), sdr["Registration"].ToString().Trim(), this);
            }
            sdr.Close();
        }

        // Stores the drivers to the local database
        public void SetDrivers()
        {
            SqlConnection connection =
                new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;");
            connection.Open();

            SqlCommand clearCommand = new SqlCommand(
                "TRUNCATE TABLE Driver;");
            clearCommand.Connection = connection;
            clearCommand.ExecuteNonQuery();

            foreach (Driver currentDriver in this.Drivers)
            {
                try
                {
                    SqlCommand command = new SqlCommand(
                        "INSERT into Driver (Name, Staff_ID, Registration) VALUES (@Name, @Staff_ID, @Registration)");
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Name", currentDriver.Name);
                    command.Parameters.AddWithValue("@Staff_ID", currentDriver.StaffID);
                    command.Parameters.AddWithValue("@Registration", currentDriver.CarRegistration);
                    command.ExecuteNonQuery();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        // Retrieves the dishes from the local database
        public void GetDishes()
        {
            SqlConnection connection =
                new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;");
            connection.Open();
            SqlCommand command = new SqlCommand(
                "SELECT Description, Vegeterian, Price FROM Dish", connection);
            SqlDataReader sdr = command.ExecuteReader();
            while (sdr.Read())
            {
                MenuItem dish = new MenuItem(sdr["Description"].ToString().Trim(), sdr["Price"].ToString(), sdr["Vegeterian"].ToString(), this);
            }
            sdr.Close();
        }

        // Stores the dishes to the local database
        public void SetDishes()
        {
            SqlConnection connection =
                new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True;");
            connection.Open();

            SqlCommand clearCommand = new SqlCommand(
                "TRUNCATE TABLE Dish;");
            clearCommand.Connection = connection;
            clearCommand.ExecuteNonQuery();

            foreach (MenuItem currentDish in this.Dishes)
            {
                try
                {
                    SqlCommand command = new SqlCommand(
                        "INSERT into Dish (Description, Vegeterian, Price) VALUES (@Description, @Vegeterian, @Price)");
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@Description", currentDish.Description);
                    command.Parameters.AddWithValue("@Vegeterian", currentDish.Vegeterian);
                    command.Parameters.AddWithValue("@Price", currentDish.Price);
                    command.ExecuteNonQuery();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }
    }
}
