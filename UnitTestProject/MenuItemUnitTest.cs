﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neil_and_Bens_Pizzeria;

namespace UnitTestProject
{
    /*    
     * The purpose of the MenuItemUnitTest is to test methods and properties of the MenuItem class.
        
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    [TestClass]
    public class MenuItemUnitTest
    {

        [TestMethod]
        public void ToPoundsTest()
        {
            // arrange
            MenuItem dish = new MenuItem();
            dish.Description = "test pizza";
            dish.Vegeterian = false;
            dish.Price = 13200;

            string expected = "£13.2";
            
            // act
            string actual = dish.ToPounds();

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PricePropertyNegativityTest()
        {
            // arrange
            MenuItem dish = new MenuItem();
            dish.Description = "test pizza";
            dish.Vegeterian = false;

            int dishPrice = -1500;
            string expectedException = "Price cannot be a negative number.";

            // act
            try
            {
                dish.Price = dishPrice;
            }

            // assert
            catch (ArgumentException e)
            {
                
                StringAssert.Contains(e.Message, expectedException);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void PricePropertyOver100PoundsTest()
        {
            // arrange
            MenuItem dish = new MenuItem();
            dish.Description = "test pizza";
            dish.Vegeterian = false;

            int dishPrice = 999999;
            string expectedException = "Price may not exceed £100 (100,000 pence)";

            // act
            try
            {
                dish.Price = dishPrice;
            }

            // assert
            catch (ArgumentException e)
            {
                
                StringAssert.Contains(e.Message, expectedException);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }
    }
}
