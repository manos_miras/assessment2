﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neil_and_Bens_Pizzeria;

namespace UnitTestProject
{
    /*  
     * The purpose of the ServerUnitTest is to test methods and properties of the Server class.
    
     * Made by Emmanuel Miras (40168970) as part of the SD2 course. 
     * Date last modified: 10/12/2015 
    */

    [TestClass]
    public class ServerUnitTest
    {
        // Tests if a SitInBill is created properly.
        [TestMethod]
        public void CreateSitInBillTest()
        {
            // arrange
            Server server = new Server();
            server.Name = "test";
            server.StaffID = 0;

            int priceParameter = 18000;
            bool isDelivery = false;
            string expectedPrice = "£18";

            // act
            string actualPrice = server.CreateBill(priceParameter, isDelivery);

            // assert
            Assert.AreEqual(expectedPrice, actualPrice);
        }

        // Tests if a DeliveryBill is created properly.
        [TestMethod]
        public void CreateDeliveryBillTest()
        {
            // arrange
            Server server = new Server();
            server.Name = "test";
            server.StaffID = 0;

            int priceParameter = 20000;
            bool isDelivery = true;
            string expectedPrice = "£23";
            // act
            string actualPrice = server.CreateBill(priceParameter, isDelivery);

            // assert
            Assert.AreEqual(expectedPrice, actualPrice);
        }

        // Tests if IsNumeric method works properly.
        [TestMethod]
        public void IsNumericTest()
        {
            // arrange
            Server server = new Server();
            server.Name = "test";
            server.StaffID = 0;

            string parameter1 = "abc";
            string parameter2 = "Not numeric";

            // act
            try
            {
                Server.IsNumeric(parameter1, parameter2);
            }

            // assert
            catch (ArgumentException e)
            {
                StringAssert.Contains(e.Message, parameter2);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }

        // Tests if IsValidID method works properly.
        [TestMethod]
        public void IsValidStaffIDTest()
        {
            // arrange
            Server server1 = new Server();
            server1.StaffID = 0;
            server1.Name = "Jack";

            Server server2 = new Server();
            server2.Name = "Jim";

            Manager manager = new Manager();
            manager.Servers.Add(server1);
            manager.Servers.Add(server2);

            string serverID = "0";
            string expectedException = "Staff ID: " + serverID + ", is already assigned to " + server1.Name;

            // act
            try
            {
                server2.IsValidID(serverID, manager);
            }

            // assert
            catch (ArgumentException e)
            {
                StringAssert.Contains(e.Message, expectedException);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }

        // Tests if the Name property works properly.
        [TestMethod]
        public void NamePropertyTest()
        {
            // arrange
            Server server = new Server();
            server.StaffID = 0;

            string serverName = "";
            string expectedException = "Server name may not be blank.";

            // act
            try
            {
                server.Name = serverName;
            }

            // assert
            catch (ArgumentException e)
            {
                StringAssert.Contains(e.Message, expectedException);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }

    }
}
